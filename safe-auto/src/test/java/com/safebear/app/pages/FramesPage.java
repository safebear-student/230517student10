package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Admin on 23/05/2017.
 */
public class FramesPage {
    static WebDriver driver;

    public FramesPage (WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public static Boolean checkCorrectPage(){
        return driver.getTitle().startsWith("Frame");
    }
}
