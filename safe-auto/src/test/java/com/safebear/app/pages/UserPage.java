package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Admin on 23/05/2017.
 */
public class UserPage {
    static WebDriver driver;

    public UserPage (WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public static Boolean checkCorrectPage(){
        return driver.getTitle().startsWith("Logged In");
    }
}
