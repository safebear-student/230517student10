package com.safebear.app;

import com.safebear.app.pages.WelcomePage;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;

/**
 * Created by Admin on 23/05/2017.
 */
public class Test01_Login extends BaseTest {

    @Test
    public void testLogin(){
        //Step 1 Confirm we're on te Welcome Page

    assertTrue(welcomePage.checkCorrectPage());

        //Step 2 Click on the Login link and the Login page loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));

        //Step 3 Login with valid credentials
        assertTrue(loginPage.login(this.userPage,"testuser","testing"));
}


}
